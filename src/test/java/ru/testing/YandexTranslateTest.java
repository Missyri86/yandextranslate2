package ru.testing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Translater;

import ru.testing.entities.Translations;
import ru.testing.gateway.TranslateGateway;

public class YandexTranslateTest {
    private static final String TEXT_TO_TRANSLATE = "Hello World!";
    private static final String LANGUAGE_CODE_EN = "en";
    private static final String LANGUAGE_CODE_RU = "ru";

    @Test
    @DisplayName("Translate to Russian")
    public void testTranslate() throws Exception {
        TranslateGateway translateGateway = new TranslateGateway();
        Translater translater = translateGateway.translate( LANGUAGE_CODE_EN, LANGUAGE_CODE_RU, TEXT_TO_TRANSLATE );
        // Translater translater = translateGateway.detectLanguage(TEXT_TO_TRANSLATE);
        Assertions.assertEquals("Всем привет!", translater.getTranslations().get(0).getText());
    }


}
