package ru.testing.entities;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Generated("jsonschema2pojo")
public class Translater {

    @SerializedName("sourceLanguageCode")
    @Expose
    private String sourceLanguageCode;
    @SerializedName("detectedLanguageCode")
    @Expose
    private String detectedLanguageCode;
    @SerializedName("languageCode")
    @Expose
    private String languageCode;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("texts")
    @Expose
    private List<String> texts = null;
    @SerializedName("folderId")
    @Expose
    private String folderId;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("glossaryConfig")
    @Expose
    private GlossaryConfig glossaryConfig;
    @SerializedName("translations")
    @Expose
    private List<Translations> translations;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("languageCodeHints")
    @Expose
    private List<String> languageCodeHints = null;


}