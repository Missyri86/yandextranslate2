package ru.testing.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Translations {
    @SerializedName("text")
    @Expose
    public String text;
}
