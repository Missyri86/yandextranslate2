package ru.testing.entities;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Generated("jsonschema2pojo")
public class GlossaryData {

    @SerializedName("glossaryPairs")
    @Expose
    private List<GlossaryPair> glossaryPairs = null;
    
}