package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Translater;
import ru.testing.entities.Translations;

@Slf4j
public class TranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/detect";
    private static final String URL_LANGUAGES = "https://translate.api.cloud.yandex.net/translate/v2/languages";
    private static final String URL_TRANSLATE = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "Api-Key AQVN0UJ7BtEQk3dJMes9WqpAdimgWreb4QdbyRR_";

    @SneakyThrows
    public Translater translate(String sourceLanguageCode, String targetLanguageCode, String texts) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL_TRANSLATE)
                .header("Authorization", TOKEN)
                .queryString("sourceLanguageCode", sourceLanguageCode)
                .queryString("targetLanguageCode", targetLanguageCode)
                .queryString("texts", texts)
                .asString();
        String strResponse = response.getBody();
        log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Translater.class);
    }

    @SneakyThrows
    public Translater detectLanguage(String text) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Authorization", TOKEN)
                .queryString("text", text)
                .asString();
        String strResponse = response.getBody();
        log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Translater.class);
    }
}
